<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class VerifyLogin extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('user','',TRUE);
 }

 function index()
 {
   //This method will have the credentials validation
   $this->load->library('form_validation');
	//these are validation rules in code igniter these will check username and password field
   $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
   $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');

   if($this->form_validation->run() == FALSE)
   {
     //Field validation failed.&nbsp; User redirected to login page
     $this->load->view('login_view');
   }
   else
   {
     //If user username and password is matching then only he will be redirected to home controller
     redirect('home', 'refresh');
   }

 }

 function check_database($password)
 {
 
   //Field validation succeeded.&nbsp; Validate against database .
   //Get username in post typed by the user and store it to a variable username
   $username = $this->input->post('username');

   //query the database
   $result = $this->user->login($username, $password);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
		//echo'<pre>';var_dump($row);die();
	 $sess_array = array(
         'id' => $row->id,
         'username' => $row->username,
         'userType' => $row->userType
       );
	   //this will set session user data from users tables . Here we have put usertype which will check weather superadmin ,administrator or user
       $this->session->set_userdata('logged_in', $sess_array);
     }
     return TRUE;
   }
   else
   {
     $this->form_validation->set_message('check_database', 'Invalid username or password');
     return false;
   }
 }
}
?>
