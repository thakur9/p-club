
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Superadmindashboard extends CI_Controller {

		function __construct(){
		
			parent::__construct();
		
		}

		function index(){
			
			$this->load->helper(array('form'));
			$this->load->view('global/sadminheader');
			$this->load->view('sAdmin/superadmindashboard');
			$this->load->view('global/footer');
		
		}
	 
	}

?>
