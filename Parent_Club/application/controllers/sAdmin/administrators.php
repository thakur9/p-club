<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Administrators extends CI_Controller {

	function __construct(){
	
		parent::__construct();//load all libraries of codeigniter
		$this->load->helper('form');
		$this->load->helper(array('form', 'url'));      
		$this->load->helper('array');	

	}

	//this is index function load default 
	function index(){
	
		$this->load->view('global/sadminheader');
		$data['posts']=$this->admini();
		$this->load->view('sAdmin/administrators',$data);
		$this->load->view('global/footer');

	}

	function admini(){
	
		$sql=$this->db->query("select * from users where userType='2'");
		$result= $sql->result();			
		return $result;
		
	}

	//this function will add new administrator 
	function addadmin(){
	
		$data = array(
		'username' => $this->input->post('username') , //get username typed by superadmin in post form
		'password' => $this->input->post('password'),	//get password in post from later it will be converted to MD5 encryption
		'userType' => '2'
		);

		$this->db->insert('users', $data); //this is a insert quarry   
		redirect('sAdmin/administrators'); //after insertion this will redirect superadmin to  administrator page of superadmin view
		
	}
	
	//this function will delete administrator 
	function delete(){
	
		$id = $this->uri->segment(4); // this will get specific administrator from url 
		$this->db->where('id', $id);
		$this->db->delete('users'); 
		redirect('sAdmin/administrators'); //redirect to view administrator page
		
	}


	}
?>
