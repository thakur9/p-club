<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Movies_And_Cartoon extends CI_Controller {

		function __construct() {
	
			parent::__construct();
			$this->load->helper('form');
			$this->load->helper(array('form', 'url'));      
			$this->load->helper('array');	

		}

		function index(){
		
			$this->load->helper(array('form'));
			$this->load->view('global/sadminheader');
			$this->load->view('sAdmin/movies_and_cartoon');
			$this->load->view('global/footer');
			
		}
	 
		function edit(){
		
			$data = array(
			'cat_name' => $this->input->post('username')
			);
			$this->db->where('id', 1);
			$this->db->update('categories', $data); 
			redirect('sAdmin/movies_and_cartoons'); 
			
		}

	}

?>
