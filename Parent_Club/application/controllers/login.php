
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

 function __construct()
 {
   parent::__construct();
 }

 function index()
 {
	//this is our default controller it will load by default.
   $this->load->helper(array('form'));
   $this->load->view('login_view');
 }

}

?>

