-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 20, 2014 at 12:26 PM
-- Server version: 5.5.8
-- PHP Version: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `parentclub`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` text NOT NULL,
  `is_show` int(11) NOT NULL DEFAULT '0',
  `url` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `cat_name`, `is_show`, `url`) VALUES
(1, 'MOVIES AND CARTOONS', 1, 'movies_and_cartoons'),
(2, 'CONSOLE GAMES', 1, 'console_games'),
(3, 'PC EDUCATIONAL SOFTWARE', 1, 'pc_educational_software'),
(4, 'TABLETS EDUCATIONAL APPS', 1, 'tablets_educational_apps');

-- --------------------------------------------------------

--
-- Table structure for table `console_games`
--

CREATE TABLE IF NOT EXISTS `console_games` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(11) NOT NULL,
  `cat_description` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `console_games`
--


-- --------------------------------------------------------

--
-- Table structure for table `movies_and_cartoon`
--

CREATE TABLE IF NOT EXISTS `movies_and_cartoon` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(70) NOT NULL,
  `cat_description` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `movies_and_cartoon`
--


-- --------------------------------------------------------

--
-- Table structure for table `pc_educational_software`
--

CREATE TABLE IF NOT EXISTS `pc_educational_software` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(701) NOT NULL,
  `cat_description` varchar(1111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pc_educational_software`
--


-- --------------------------------------------------------

--
-- Table structure for table `pc_sessions`
--

CREATE TABLE IF NOT EXISTS `pc_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pc_sessions`
--

INSERT INTO `pc_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('44b2f49f02b333b3987227676767bac8', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:27.0) Gecko/20100101 Firefox/27.0', 1392898656, 'a:2:{s:9:"user_data";s:0:"";s:9:"logged_in";a:3:{s:2:"id";s:1:"3";s:8:"username";s:5:"admin";s:8:"userType";s:1:"2";}}');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE IF NOT EXISTS `sub_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_cat_name` varchar(70) NOT NULL,
  `is_show` int(11) NOT NULL DEFAULT '0',
  `url` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `sub_cat_name`, `is_show`, `url`) VALUES
(1, 'All Ages', 1, 'all_ages'),
(2, 'Preschoolers (2-4)', 1, 'preschoolers'),
(3, 'Young kids (5-6)', 1, 'young_kids'),
(4, 'Kids (7-8)', 1, 'kids'),
(5, 'Preteens (9-11)', 1, 'preteens'),
(6, 'Teens (12-14)', 0, 'teens');

-- --------------------------------------------------------

--
-- Table structure for table `tablet_educational_app`
--

CREATE TABLE IF NOT EXISTS `tablet_educational_app` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(70) NOT NULL,
  `cat_description` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tablet_educational_app`
--


-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `username` varchar(10) NOT NULL,
  `password` varchar(100) NOT NULL,
  `userType` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `userType`) VALUES
(2, 'bob', '9a618248b64db62d15b300a07b00580b', 0),
(3, 'admin', '21232f297a57a5a743894a0e4a801fc3', 2),
(4, 'sadmin', 'c5edac1b8c1d58bad90a246d8f08f53b', 1),
(5, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 3),
(6, 'Shailender', 'f96b81b53d2187d668a4e69f37cbb414', 2),
(7, 'Shail', 'shail', 1),
(8, 'Shailen', 'f96b81b53d2187d668a4e69f37cbb414', 2),
(9, 'Shail', 'shail', 1),
(21, 'thakur9', 'admin', 2),
(20, 'nikhil', 'nikhil123', 2),
(19, 'samar', 'samar', 2);
