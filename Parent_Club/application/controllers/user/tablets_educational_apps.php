<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tablets_Educational_Apps extends CI_Controller {


	function index()
	{
		$this->load->helper(array('form'));
	    $this->load->view('global/header');
		$data['posts']=$this->sub_category();
		$this->load->view('user/tablets_educational_apps',$data);
		$this->load->view('global/footer');
	}
	 
	function sub_category(){
		$sql=$this->db->query("select * from sub_categories where is_show='1'");
		$result= $sql->result();			
		return $result;
	}
}
?>
      