<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class console_games extends CI_Controller {

 function index()
 {
	$this->load->helper(array('form'));
	$this->load->view('global/header');
	$this->load->view('user/console_games');
	$this->load->view('global/footer');
 }
}
?>
