<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Movies_And_Cartoon extends CI_Controller {

 function __construct()
 {
   parent::__construct();
 }

 function index()
 {
	$this->load->helper(array('form'));
	$this->load->view('global/header');
	$this->load->view('user/movies_and_cartoon');
	$this->load->view('global/footer');
 }
 

}

?>
