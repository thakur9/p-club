<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pc_Educational_Software extends CI_Controller {
function __construct()
 {
   parent::__construct();
 }
 function index()
 {
	$this->load->helper(array('form'));
	$this->load->view('global/header');
	$this->load->view('user/pc_educational_softwares');
	$this->load->view('global/footer');
 }
 
}

?>
