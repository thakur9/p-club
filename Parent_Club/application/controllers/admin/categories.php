
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Categories extends CI_Controller {

		function __construct(){
		
			parent::__construct();
		}

		function index(){
		
		$this->load->helper(array('form')); //this is a helper file it will pull data in form 
		$this->load->view('global/adminheader');
		$data['posts']=$this->category();   /// this will get all data from category function 
		$this->load->view('admin/categories',$data);
		$this->load->view('global/footer');

		}

		//this function will load all categories list  
		function category(){
			$sql=$this->db->query("select * from categories where is_show='1'");
			$result= $sql->result();			
			return $result;
		}
	}

?>
