<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sub_Category extends CI_Controller {

	function __construct() {
	
	   parent::__construct();
	   
	 }

	function index(){
	
		$this->load->view('global/adminheader');
		$data['posts']=$this->sub_cat(); 
		$this->load->view('admin/sub_category',$data);
		$this->load->view('global/footer');

	}
	
	//this function will load all subcategories 
	function sub_cat(){
		
		$sql=$this->db->query("select * from sub_categories where is_show='1'");
		$result= $sql->result();			
		return $result;
	}
}
?>
