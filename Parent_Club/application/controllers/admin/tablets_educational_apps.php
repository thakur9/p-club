<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tablets_Educational_Apps extends CI_Controller {

	function __construct() {
	
	   parent::__construct();
	   
	}
	
			function index() { //this will load default console games category
		
			$this->load->helper(array('form'));
			$this->load->view('global/adminheader');
			$this->load->view('admin/tablets_educational_apps');
			$this->load->view('global/footer');
			
		}
	}

?>
