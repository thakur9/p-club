<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class console_games extends CI_Controller {

		function index() { //this will load default console games category
		
			$this->load->helper(array('form'));
			$this->load->view('global/adminheader');
			$this->load->view('admin/console_games');
			$this->load->view('global/footer');
			
		}
	}
?>
