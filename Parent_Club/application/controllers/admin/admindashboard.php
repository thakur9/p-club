
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Admindashboard extends CI_Controller {

		function __construct() {
		
			parent::__construct();
		}
		
		//this will load defaut when user is a administrator
		function index() {
		
			$this->load->helper(array('form'));
			$this->load->view('global/adminheader');
			$this->load->view('admin/admindashboard');
			$this->load->view('global/footer');
		}

	}

?>
