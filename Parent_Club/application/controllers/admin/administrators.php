<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administrators extends CI_Controller {

	 function __construct(){
	 
	   parent::__construct();
	 }

	function index(){
	
		$this->load->view('global/adminheader');
		// this data['posts'] load all administrators and send them to view administrators page
		$data['posts']=$this->admini();
		$this->load->view('admin/administrators',$data);
		$this->load->view('global/footer');

	}
	
	// this will get all administrator from the users table 
	function admini(){
	
		$sql=$this->db->query("select * from users where userType='2'");
		$result= $sql->result();			
		return $result;
	}
}
?>
