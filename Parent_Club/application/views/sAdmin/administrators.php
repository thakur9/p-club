<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
	<h3 class="page-title">
		Administrators		
	
	</h3>
	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="<?php echo base_url();?>index.php/sAdmin/superadmindashboard">Home</a> 
			<i class="icon-angle-right"></i>
		</li>
		<li><a href="<?php echo base_url();?>index.php/sAdmin/administrators">Administrator</a></li>
	</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<span>
	<!-- Attach our CSS -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/reveal.css">	  	
	<!-- Attach necessary scripts -->
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/css/jquery.reveal.js"></script>
<a href="#" class="big-link" data-reveal-id="myModal">Add Administrator</a>

 <div id="myModal" class="reveal-modal">
			        <div class="modal-header">  
            <h3>Add Administrator</h3>  
        </div>      
        <div class="modal-body">  
			<div class="container">
			<form role="form" class="form-signin" action="<?php base_url();?>administrators/addadmin" method="POST">
			<input type="text" name="username" autofocus="" required="" placeholder="Admin Username" class="form-control">
			<input type="password" required="" placeholder="Password" name="password" class="form-control">
			<button type="submit" class="btn">ADD</button>
			</form>
			</div>
        </div>  
		</div>
</span>	
<div id="content" class="content full">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 center-col">
				<div class="row clean-top featurd-product">
				
				<?php
					foreach($posts as $ra){
						$this->strc.='<div class="col-lg-4 box-contain"><div class="center-white"><p>'.$ra->username.'</p>';				
					$this->strc.='<p>
					<a href="'.site_url("sAdmin").'/administrators/delete/'.$ra->id.'" class="btn btn-danger">delete</a>
					</p></div></div>';
					} 
					echo $this->strc;
					
				?>			
				</div>
			</div>
		</div>
	</div>
</div>
  