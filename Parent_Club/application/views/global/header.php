<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	
<!-- Mirrored from www.crivosthemes.com/theme/porto/ by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 04 Dec 2013 11:14:29 GMT -->
<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<title>Parent Club</title>
		<meta name="keywords" content="" />
		<meta name="description" content="">
		<meta name="author" content="">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Libs CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.css">
		<link rel="stylesheet" href="<?php echo base_url();?>fonts/font-awesome/css/font-awesome.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>css/theme.css">

		<!-- Responsive CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>css/theme-responsive.css" />

		<!-- Head Libs -->
		<script src="<?php echo base_url();?>js/modernizr.js"></script>

		<!--[if IE]>
			<link rel="stylesheet" href="css/ie.css">
		<![endif]-->

		<!--[if lte IE 8]>
			<script src="vendor/respond.js"></script>
		<![endif]-->

	</head>
	<body >
<div class="body">
	<header class="clean-top center">
		<div class="header-top">
			<div class="container">
			 <div class="col-md-4 left-bttn">
				<p class="deals-day"><a href="#">Movie of the day</a></p>
			   </div>
			   <div class="col-md-8 right-nav">
				<nav class="top-center">
				<ul class="navs nav-pills nav-top">
					<li><a href="#">My Account</a></li>
					<li><a href="<?php echo base_url();?>index.php/home/logout">Logout</a></li>
				</ul>
			</nav>
			
			  </div> 
			</div>
		 </div>
		<div class="header-top-2"> 
		 <div class="container">
		  <div class="col-md-6 left-logo">
			<h1 class="logo">
				<a href="index-2.html">
					<img alt="Porto" src="img/logo.png">
				</a>
			</h1>
		 </div>
		 <div class="col-md-6 right-logo">
		 <div class="navbar-collapse nav-main-collapse collapse">
			<div class="search-bar">
			  <input name="search" type="text" class="seacrh-div" id="search" placeholder="Search here..">
			  <span class="input-group-btn-search">
				<button class="btn btn-default" type="button" value="Search">SEARCH</button>
			 </span>
			</div>
		   </div>
		  </div>
		 </div>
		</div> 
	   </header> 
<div class="container">    
<button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target="nav-main-collapse">
	<p class="icon icon-bars">MENU</p>
</button>
</div>
<div class="navbar-collapse nav-main-collapse collapse back-nav " >
	<div class="container">
	  <nav class="nav-main">
	  <ul class="nav nav-pills nav-main" id="mainMenu" style="margin-left:24%">
	  <?php
			$sql=$this->db->query("select * from sub_categories where is_show='1'");
			$result= $sql->result();			
			foreach($result as $r)
			{
				$this->str.='<li class="dropdown"><a class="dropdown-toggle" href="'.base_url().''.$r->url.'">'.$r->sub_cat_name.'</a></li>';				
			} 
				echo $this->str;
		  ?>
		</ul>
	</nav>
</div>
</div>

<div role="main" class="main">
	<div id="content" class="content full">
	  <div class="container">
	   <div class="row">
		<div class="col-lg-2 left-col">
		 <div class="category-outer">
		  <h2>Category</h2>
		  <?php
			$sql=$this->db->query("select * from categories where is_show='1'");
			$result= $sql->result();			
			foreach($result as $r)
			{
				$this->strc.='<ul class="category-div"><li><a href="'.$r->url.'">'.$r->cat_name.'</a></li></ul>  ';				
			} 
				echo $this->strc;
		  ?>		  
		</div>
	   </div> 
		<div class="col-lg-8 center-col">
		</div>
	  </div>
	</div>
</div>   