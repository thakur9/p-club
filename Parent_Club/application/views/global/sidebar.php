<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        	
			<ul>
				<li>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler hidden-phone"></div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				</li>
				<li>
					<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
					<form class="sidebar-search">
						<div class="input-box">
							<a href="javascript:;" class="remove"></a>
							<input type="text" placeholder="Search..." />				
							<input type="button" class="submit" value=" " />
						</div>
					</form>
					<!-- END RESPONSIVE QUICK SEARCH FORM -->
				</li>
				<?php if($_SERVER["PATH_INFO"]=="/sAdmin/superadmindashboard"){	?>
				<li class="start active ">
				<?php }else{ ?>	<li class=""><?php } ?>
					<a href="<?php echo base_url();?>index.php/sAdmin/superadmindashboard">
					<i class="icon-home"></i> 
					<span class="title">Dashboard</span>
					<span class="selected"></span>
					</a>
				</li>
				<?php if($_SERVER["PATH_INFO"]=="/sAdmin/categories"){	?>
				<li class="start active ">
				<?php }else{ ?>	<li class=""><?php } ?>
					<a href="<?php echo base_url();?>index.php/sAdmin/categories">
					<i class="icon-user"></i> 
					<span class="title">Category</span>
					<span class="selected"></span>
					</a>
				</li>
				<?php if($_SERVER["PATH_INFO"]=="/sAdmin/sub_category"){	?>
				<li class="start active ">
				<?php }else{ ?>	<li class=""><?php } ?>
					<a href="<?php echo base_url();?>index.php/sAdmin/sub_category">
					<i class="icon-user"></i> 
					<span class="title">Sub Category</span>
					<span class="selected"></span>
					</a>
				</li>
				<?php if($_SERVER["PATH_INFO"]=="/sAdmin/administrators"){	?>
				<li class="start active ">
				<?php }else{ ?>	<li class=""><?php } ?>
					<a href="<?php echo base_url();?>index.php/sAdmin/administrators">
					<i class="icon-user"></i> 
					<span class="title">Administrator</span>
					<span class="selected"></span>
					</a>
				</li>
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->